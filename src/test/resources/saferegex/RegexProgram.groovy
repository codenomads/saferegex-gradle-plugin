import java.util.regex.Pattern

/**
 * A sample program with safe regex patterns
 */
class RegexProgram {
    private static final Pattern OK_PATTERN = Pattern.compile('[abc]')
    private static final Pattern GOOD_PATTERN = Pattern.compile('abc|def')
}
