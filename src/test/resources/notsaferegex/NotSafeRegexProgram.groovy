import java.util.regex.Pattern

/**
 * A sample program with safe and non-safe regex patterns
 */
class NotSafeRegexProgram {
    private static final Pattern OK_PATTERN = Pattern.compile('[abc]')
    private static final Pattern BAD_PATTERN = Pattern.compile('(.*a){12}')
    private static final Pattern GOOD_PATTERN = Pattern.compile('abc|def')
    private static final Pattern UNSAFE_PATTERN = Pattern.compile('^[.a-zA-Z_]{0,30}\$')
}
