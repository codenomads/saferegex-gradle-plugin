package nl.codenomads.plugin.saferegex

import org.junit.Before
import org.junit.Test
import org.junit.rules.TemporaryFolder

import static org.gradle.testkit.runner.TaskOutcome.FAILED
import static org.gradle.testkit.runner.TaskOutcome.SUCCESS
import static org.junit.Assert.assertEquals

/**
 * Contains functional tests that use the GradleRunner to run the plugin's task in a controlled environment.
 * Reference:
 * https://docs.gradle.org/4.10.2/userguide/test_kit.html#sec:functional_testing_with_the_gradle_runner
 */
class SafeRegexModuleTest extends SafeRegexModuleAbstractTest {

    @Before
    void setupModuleTest() {
        setup()
    }

    @Test
    void saferegex_standard() {
        def result = gradle( false, 'saferegexScan')
        assertEquals("Expected saferegexScan to fail", FAILED, result.task(":saferegexScan").outcome)
    }

    @Test
    void saferegex_alternative_src_path() {
        final File resourcesFolder = new SafeRegexTestUtil().getResourcesFile("saferegex/RegexProgram.groovy").getParentFile()
        build_gradle << 'saferegex.alternativeScanPath = "' + resourcesFolder.path + '"\n'
        def result = gradle( 'saferegexScan')
        assertEquals("Expected saferegexScan to succeed", SUCCESS, result.task(":saferegexScan").outcome)
    }

    @Test
    void saferegex_alternative_src_path_not_safe() {
        final File resourcesFolder = new SafeRegexTestUtil().getResourcesFile("notsaferegex/NotSafeRegexProgram.groovy").getParentFile()
        build_gradle << 'saferegex.alternativeScanPath = "' + resourcesFolder.path + '"\n'
        def result = gradle(false, 'saferegexScan')
        assertEquals("Expected saferegexScan to fail", FAILED, result.task(":saferegexScan").outcome)
    }

    @Override
    TemporaryFolder getTemporaryFolder() {
        return testProjectDir
    }
}
