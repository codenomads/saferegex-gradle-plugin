package nl.codenomads.plugin.saferegex

import org.junit.Test

import static org.assertj.core.api.Assertions.assertThat
import static org.junit.Assert.assertEquals
import static org.junit.Assert.assertFalse
import static org.junit.Assert.assertTrue

/**
 * Unit test of {@link SafeRegexUtil}
 */
class SafeRegexUtilTest {

    private SafeRegexTestUtil safeRegexTestUtil = new SafeRegexTestUtil()

    @Test
    void testRegexParseNoPatternCompile() {
        final File settingsFile = safeRegexTestUtil.getResourcesFile("saferegex/app-example.properties")
        final Set<String> regexesFound = SafeRegexUtil.extractRegex(settingsFile)
        assertThat(regexesFound).hasSize(0)
    }

    @Test
    void testRegexParsePatternCompileJavaFile() {
        final File settingsFile = safeRegexTestUtil.getResourcesFile("saferegex/RegexProgram.groovy")
        final Set<String> regexesFound = SafeRegexUtil.extractRegex(settingsFile)
        assertThat(regexesFound).hasSize(2)
    }

    @Test
    void testRegexParsePatternCompileJavaFileNotSafe() {
        final File settingsFile = safeRegexTestUtil.getResourcesFile("notsaferegex/NotSafeRegexProgram.groovy")
        final Set<String> regexesFound = SafeRegexUtil.extractRegex(settingsFile)
        assertThat(regexesFound).hasSize(4)
    }


    @Test
    void testValidateRegexDirectorySafe() {
        final File safeRegexFile = safeRegexTestUtil.getResourcesFile("saferegex/RegexProgram.groovy")
        final Set<String> regexesFound = SafeRegexUtil.extractRegex(safeRegexFile)
        assertEquals("Expected four two regexes", 2, regexesFound.size())

        final File safeRegexDir = safeRegexFile.getParentFile()
        assertTrue("Expected valid regexes in directory", SafeRegexUtil.validateRegexes(safeRegexDir))
    }

    @Test
    void testValidateRegexDirectoryNotSafe() {
        final File notSafeRegexFile = safeRegexTestUtil.getResourcesFile("notsaferegex/NotSafeRegexProgram.groovy")
        final Set<String> regexesFound = SafeRegexUtil.extractRegex(notSafeRegexFile)
        assertEquals("Expected four two regexes", 4, regexesFound.size())

        final File notsafeRegexDir = notSafeRegexFile.getParentFile()
        assertFalse("Expected not valid regexes in directory", SafeRegexUtil.validateRegexes(notsafeRegexDir))
    }
}
