package nl.codenomads.plugin.saferegex


import org.gradle.testkit.runner.BuildResult
import org.gradle.testkit.runner.GradleRunner
import org.junit.Rule
import org.junit.rules.TemporaryFolder

/**
 * Base abstract class for the Sonar Quality Gate Check Gradle plugin test
 * using the {@link GradleRunner}
 */
abstract class SafeRegexModuleAbstractTest {
    @Rule
    public final TemporaryFolder testProjectDir = new TemporaryFolder()

    protected File build_gradle
    private static final String[] DEFAULT_ARGUMENTS = ['tasks']

    /**
     * Initialise the build.gradle file
     */
    void setup() {
        // Prepare build.gradle
        build_gradle = getTemporaryFolder().newFile('build.gradle')
        build_gradle << """
            plugins {
                id 'nl.codenomads.plugin.SafeRegexPlugin'
            }
        """.stripIndent()
    }

    /**
     * @return the {@link TemporaryFolder} to tbe use for the unit tests
     */
    abstract TemporaryFolder getTemporaryFolder()

    /**
     * Helper method that runs a Gradle task in the testProjectDir
     * @param arguments the task arguments to execute
     * @param isSuccessExpected boolean representing whether or not the build is supposed to fail
     * @return the task's BuildResult
     */
    protected BuildResult gradle(boolean isSuccessExpected, String[] arguments = DEFAULT_ARGUMENTS) {
        arguments += '--stacktrace'
        def runner = GradleRunner.create()
                .withArguments(arguments)
                .withProjectDir(getTemporaryFolder().root)
                .withPluginClasspath()
                .withDebug(true)
        return isSuccessExpected ? runner.build() : runner.buildAndFail()
    }

    /**
     * Helper method that runs a Gradle task in the testProjectDir
     * @param arguments the task arguments to execute
     * @return the task's BuildResult
     */
    protected BuildResult gradle(final String[] arguments = DEFAULT_ARGUMENTS) {
        gradle(true, arguments)
    }
}
