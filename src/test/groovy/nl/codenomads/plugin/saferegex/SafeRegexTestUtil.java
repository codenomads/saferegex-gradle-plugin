package nl.codenomads.plugin.saferegex;

import java.io.File;

/**
 * Utility class for SafeRegex unit tests
 * Note: methods not static due to getClass()
 */
public class SafeRegexTestUtil {
    /**
     * Return the {@link File} from the resources folder for the given {@link String} name
     * @param name the {@link String} name to use
     * @return the {@link File} found
     */
    File getResourcesFile(final String name) {
        return new File(getClass().getClassLoader().getResource(name).getFile());
    }
}
