package nl.codenomads.plugin.saferegex

/**
 * An extension object for the Sonar Quality Gate Check module which allows the following to be
 * configured:
 *    alternative Scan Path to search for regular expressions
 *
 * Reference:
 * https://docs.gradle.org/4.10.2/userguide/custom_plugins.html#sec:getting_input_from_the_build
 */
class SafeRegexExtension {
    String alternativeScanPath
}

