package nl.codenomads.plugin.saferegex


import org.gradle.api.GradleException
import org.gradle.api.Project

/**
 * The "module" isn't anything Gradle specific.
 * It's just a way of grouping tasks and configuration that share a certain theme.
 * The module's "load" method is called in the plugin's entry point at {@link nl.codenomads.plugin.SafeRegexPlugin}
 */
class SafeRegexModule {

    static void load(Project project) {
        /*
         * Register a 'saferegex' extension, with the properties defined in SonarQualityGateCheckExtension
         * Reference:
         * https://docs.gradle.org/4.10.2/userguide/custom_plugins.html#sec:getting_input_from_the_build
         */
        project.extensions.create("saferegex", SafeRegexExtension)

        /*
        * A task using a project property for configuration.
        * Reference:
        * https://docs.gradle.org/4.10.2/userguide/build_environment.html#sec:gradle_configuration_properties
        */
        project.task('saferegexScan') {
            group = "saferegex"
            description = "A plugin that scans for insecure regular expressions"

            doFirst {
                final String scanPath = project.extensions.saferegex.alternativeScanPath ?: "${project.rootDir.path}/src/main/".toString()
                println "Testing SafeRegex Scan Path Exists: $scanPath"
                if (!new File(scanPath).exists()) {
                    throw new GradleException("Property saferegex.alternativeScanPath does not exist")
                }
            }

            doLast {
                final String scanPath = project.extensions.saferegex.alternativeScanPath ?: "${project.rootDir.path}/src/main/".toString()
                println "SafeRegex Scan Path: $scanPath"
                if (!SafeRegexUtil.validateRegexes(new File(scanPath))) {
                    throw new GradleException("SafeRegexScan: Vulnerable Regex found")
                }
            }
        }
    }
}
