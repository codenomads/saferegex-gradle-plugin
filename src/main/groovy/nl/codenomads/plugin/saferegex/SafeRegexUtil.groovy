package nl.codenomads.plugin.saferegex


import java.util.function.Predicate
import java.util.regex.Pattern
import java.util.stream.Collectors

import static groovy.io.FileType.FILES
import static org.saferegex.RegexTester.isVulnerable

class SafeRegexUtil {

    private static Pattern javaRegex = Pattern.compile('Pattern\\.compile\\((.+)\\)')

    private SafeRegexUtil() {
        // default constructor hidden for utility class
    }

    /**
     * Extract the {@link Set} of {@link String} regular expressions for the given {@link File}
     * @param file the {@link File} to use
     * @return the {@link Set} of {@link String} regular expressions found
     */
    static Set<String> extractRegex(final File file) {
        def regexesFound = new HashSet<String>()
        file.eachLine { line ->
            def matcher = javaRegex.matcher(line)
            while (matcher.find()) {
                regexesFound.add(matcher.group(1))
            }
        }
        return regexesFound
    }

    /**
     * Validate the given {@link File} directory for non vulnerable regular expressions.
     * @param directory the {@link File} directory to traverse for Java source files
     * @return true of {@link File} directory has no vulnerabilities, false otherwise
     */
    static boolean validateRegexes(final File directory) {
        def unSafeRegexesDir = []

        directory.traverse(type: FILES, nameFilter: ~/.*\.groovy$/) {
            def unSafeRegexes = extractRegex(it).stream()
                                .filter { regex -> isVulnerable(regex) }.collect()
            if (!unSafeRegexes.isEmpty()) {
                println "${it.name} contains unsafe regular expressions"
                unSafeRegexes.every { println "${it}" }
                unSafeRegexesDir.addAll(unSafeRegexes)
            }
        }
        return unSafeRegexesDir.isEmpty()
    }
}
