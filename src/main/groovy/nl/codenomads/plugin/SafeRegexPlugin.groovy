package nl.codenomads.plugin


import nl.codenomads.plugin.saferegex.SafeRegexModule
import org.gradle.api.Plugin
import org.gradle.api.Project

/**
 * The plugin's entry point.
 * Reference:
 * https://docs.gradle.org/4.10.2/userguide/custom_plugins.html#sec:writing_a_simple_plugin
 */
class SafeRegexPlugin implements Plugin<Project> {

    /**
     * Applies this plugin to the given Gradle project
     * @param project The Gradle project
     */
    void apply(Project project) {
        // Apply all functionality from the SafeRegexModule
        SafeRegexModule.load(project)
    }
}
