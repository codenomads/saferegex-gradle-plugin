# Introduction

This is a Gradle plugin to check the regular expression usage in a project that is it safe from Regular Expression Denial of Service [ReDos](https://www.owasp.org/index.php/Regular_expression_Denial_of_Service_-_ReDoS) attacks. 

# Prior Work

This is inspired by [Joe Kutner's](https://github.com/jkutner) work on [SafeRegex](https://github.com/jkutner/saferegex) and also uses this as a dependency.

This plugin is inspired from the Gradle Plugin Bootstrap [blog post on Praqma.com](http://www.praqma.com/stories/gradle-plugin-bootstrap) and associated source code at [GitHub](https://github.com/Praqma/gradle-plugin-bootstrap).

## Developer Information

To run and build together with the unit tests and publish to the local Maven repository:
```bash
./gradlew --console=plain clean build test publishToMavenLocal
```

To run unit tests in your IDE like IntelliJ it can be necessary to first run:
```bash
./gradlew pluginUnderTestMetadata 
```

To publish the plugin you can run:
```bash
./gradlew login
./gradlew --console=plain clean build test publishPlugins
```

## Usage

To use this plugin add the following to the build.gradle 

```groovy
plugins {
    ...
    id 'nl.codenomads.plugin.SafeRegexPlugin' version '0.0.1'
    ...
}

saferegex {
  alternativeScanPath '/a/nother/path'
}
```

The following command is used to use this plugin:

```bash
./gradlew saferegexScan
```
